$(document).ready(function() {

  // Scrollbar
  //$('.scroll,.message-scroll').slimScroll();
  // Tabs

  $('[data-toggle="dropdown"]').dropdown();

  $('[data-toggle="tooltip"]').tooltip({
    container:'body',
  });

  $('[data-toggle="lone-tooltip"]').tooltip({
      container:'body'
  });

  $('[data-toggle="lone-tooltip"]').on('shown.bs.tooltip',function(e){
      $('[data-toggle="tooltip"]').not(this).tooltip('hide');
  });

  $("#yourModal").modal("hide");

  $('.tabs li:first a').addClass('active');

  $(".tabs a").click(function(){
      $('.tabs a.active').not(this).removeClass('active');
      $(this).toggleClass('active');
   });

   $('.input-group.date input').datepicker({
     changeMonth: true,
     changeYear: true
    });

   // tables
   $('.datatable').DataTable( {
        "scrollX": true
    } );

  // Mobile nav

  $("#nav-icon").click(function(e) {
      $('#nav-icon').toggleClass('active');
      $('html').toggleClass('fix');
      $('#sidenav').toggleClass('active');
      $('#sidenav').fadeToggle();
      $('#header .search').fadeToggle();
      e.preventDefault();
   });


   //Drag

});
