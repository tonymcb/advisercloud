var gulp = require('gulp');
// Requires the gulp-sass plugin
var postcss = require('gulp-postcss');
var sass = require('gulp-sass')(require('sass'));
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var nunjucksRender = require('gulp-nunjucks-render');


gulp.task('sass', function () {
  return gulp
    .src('src/scss/**/*.scss')
    .pipe(sass({
    }).on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./')); // output to theme root
});

gulp.task('js', function () {
  return gulp
    .src(['node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/tether/dist/js/tether.min.js',
    'node_modules/bootstrap/js/dist/collapse.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-dt/js/dataTables.dataTables.min.js'])
    .pipe(gulp.dest('./'));
});

gulp.task('icons', function () {
  return gulp
    .src(['node_modules/bootstrap-icons/**'])
    .pipe(gulp.dest('src'));
});

gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src('pages/**/*.+(html|nunjucks)')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['templates'],
      watch: true,
    }))
  // output files in app folder
  .pipe(gulp.dest('./'))
});


gulp.task('watch', function(){
  gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('./style.css').on('change', browserSync.reload);
  gulp.watch('./*.php').on('change', browserSync.reload);
  gulp.watch('pages/**/*.+(html|nunjucks)', gulp.series('nunjucks'));
  gulp.watch('templates/**/*.+(html|nunjucks)', gulp.series('nunjucks'));
  // Other watchers
});
